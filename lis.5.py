# Создайте массив состоящий из словарей с данными
arr = [
    {
        'name': 'Adil',
        'age': 17
    },
    {
        'name': 'Malik',
        'age': 22
    },
    {
        'name': 'Jack',
        'age': 19
    }
]

# напишите функцию которая фильтрует массив
arr = [
    {
        'name': 'Adil',
        'age': 17
    },
    {
        'name': 'Malik',
        'age': 22
    },
    {
        'name': 'Jack',
        'age': 19
    }
]

def prinemaet(vozrast):
    newMass = []
    for now in vozrast:
        if now['age'] >= 18:
            newMass.append(now)

    return newMass

prinemaetMass = prinemaet(arr)

# Напишите функцию которая принимает отфильтрованные данные, добавляет
# новое значение каждому из элементов отфильтрованных данных и возвращает
# измененные данные с добавленными значениями
def povtor(vozrosta):
    for hol in vozrosta:
        hol["surname"] = "Daniels"
    return vozrosta


addedKeys = povtor(prinemaetMass)
print(addedKeys)

# Напишите функцию которая принимает массив ранее измененых данных,
# меняет значение в каждом из элементов и возращает измененные данные
def snova(chislo):
    for lol in chislo:
        lol['age'] += 2

    return chislo


print(*snova(addedKeys))

# Напишите функцию которая принимает массив ранее измененых данных,
# и поочередно выводит их в консоль
def opat(prinat):
    for vdv in prinat:
        import time

        print(vdv)
        time.sleep(2)

print(opat(addedKeys))
